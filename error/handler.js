
function clientErrorHandler (err, req, res, next) {
  console.log(err);
    if (req.xhr) {
      res.status(500).send({ error: 'Something failed!' })
    } else {
      next(err)
    }
  }
  function errorHandler (err, req, res, next) {
    console.log(err);
    res.status(500)
    return res.json('error', { error: err })
  }
  function logErrors (err, req, res, next) {
    console.log(err);
    console.error(err?.stack)
    next(err)
  }

  module.exports = {
    clientErrorHandler,
    errorHandler,
    logErrors
  }