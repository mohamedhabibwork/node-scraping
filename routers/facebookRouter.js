const express = require('express');
const router = new express.Router();
const puppeteer = require('puppeteer');
const email = process.env.USER_EMAIL || "facescrpe@gmail.com";
const password = process.env.USER_PASSWORD || "aaaaasssss";

const useBrowser = async () => {
    try {
        return await puppeteer.launch({
            headless: 'new',
            executablePath: process.env.PUPPETEER_EXECUTABLE_PATH || '/usr/bin/chromium-browser',
            args: [
                '--no-sandbox',
                '--disabled-setupid-sandbox',
                '--disable-setuid-sandbox',
                '--disable-extensions',
                '--ignore-certifcate-errors',
                '--ignore-certifcate-errors-spki-list',
                '--ignoreHTTPSErrors=true',
            ],
            timeout: 0,
            slowMo: 0,
            devtools: false,
        });
    } catch (error) {
        console.log({stack: error.stack});
    }
}
router.get('/posts', (req, res) => {
    return res.send('hello');
});

router.post('/posts', async (req, res) => {
    try {
        const browser = await useBrowser();
        if (!browser) {
            return res.send('Browser not found');
        }
        let page = await browser.newPage();
        await page.setDefaultNavigationTimeout(0);

        await page.goto("https://www.facebook.com/", {timeout: 0});
        await page.type('input[name=email]', email);
        await page.type("input[name=pass]", password);
        await page.waitForSelector("button[type=submit]");
        await page.click("button[type=submit]");
        await page.waitForNavigation();

        let tmp = await page.evaluate(() => {
            if (document.querySelector("input[name=email]")) {
                return 0;
            }
            return 1;
        });
        if (tmp == 0) {
            return {
                error: "Unauthenticated"
            };
        }
        let xdata = [];

        if (req.body.sort == "posts") {
            xdata = await get_posts(req, browser, page);
        } else if (req.body.sort == "latest_posts") {
            xdata = await get_latest_posts(req, browser, page);
        } else if (req.body.sort == "videos") {
            xdata = await get_videos(req, browser, page);
        }
        await browser.close();
        return res.send(xdata);
    } catch (e) {
        console.log(e);
        return res.json({x: 'timeout12', error: e, stack: e?.stack});
    }
});

async function get_videos(req, browser, page) {

    await page.goto("https://www.facebook.com/search/videos/?q=" + req.body.keyword, {
        waitUntil: "networkidle2",
        timeout: 0,
    });

    let sel = "div[class='x1i10hfl xjbqb8w x6umtig x1b1mbwd xaqea5y xav7gou x9f619 x1ypdohk xt0psk2 xe8uvvx xdj266r x11i5rnm xat24cr x1mh8g0r xexx8yu x4uap5 x18d9i69 xkhd6sd x16tdsg8 x1hl2dhg xggy1nq x1a2a7pz xt0b8zv xzsf02u x1s688f']";

    // let sel2 = "[role=article] > div > div > div > div > div > div:nth-child(2) > div > div > div:nth-child(3)";
    // let sel3 = "[role=article] span[id] span[class='x4k7w5x x1h91t0o x1h9r5lt x1jfb8zj xv2umb2 x1beo9mf xaigb6o x12ejxvf x3igimt xarpa2k xedcshv x1lytzrv x1t2pt76 x7ja8zs x1qrby5j'] a[class='x1i10hfl xjbqb8w x6umtig x1b1mbwd xaqea5y xav7gou x9f619 x1ypdohk xt0psk2 xe8uvvx xdj266r x11i5rnm xat24cr x1mh8g0r xexx8yu x4uap5 x18d9i69 xkhd6sd x16tdsg8 x1hl2dhg xggy1nq x1a2a7pz x1heor9g xt0b8zv xo1l8bm']";
    let count = 0;
    let sdata = [];
    page.on('console', async (msg) => {
        const msgArgs = msg.args();
        for (let i = 0; i < msgArgs.length; ++i) {
            console.log(await msgArgs[i].jsonValue());
        }
    });
    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    let searchLimit = req.body.searchLimit;
    while(count < searchLimit) {
        let x = await page.evaluate(async () => {
            window.scrollBy(0, 100);
            if (document.querySelector("[role=feed] [class='x1n2onr6 x1ja2u2z x9f619 x78zum5 xdt5ytf x2lah0s x193iq5w xz9dl7a']")) {
                return 0;
            }
            if(document.querySelector("img[src='https://static.xx.fbcdn.net/rsrc.php/y_/r/Krj1JsX3uTI.svg?_nc_eui2=AeFlHmecVHfBL0GiEFOTScAfejgaiNC5All6OBqI0LkCWcWpgMfkyH4KFDta9BQPSGrdYyY4wuznAO3z7VlIjomN']")) {
                return 0;
            }

            return 1;
        });

        if(x == 0) {
            break;
        }

        await sleep(200);

        await page.evaluate(async (sel) => {
            let elements = Array.from(document.querySelectorAll(sel));
            elements.forEach((item) => {
                item.click();
            });
        }, sel);

        ({count, sdata} = await page.evaluate(() => {
            let all_data = [];
            all_data = Array.from(document.querySelectorAll("[role=article]")).map((item) => {
                let data = {};
                let title = item.querySelector("div[class='x78zum5 xdt5ytf xz62fqu x16ldp7u'] > div[class='xu06os2 x1ok221b'] a")
                data.text = title.textContent;
                data.url = title.href;
                return data;
            })

            let count = all_data.length;
            return {count, sdata: all_data};

        }));
        // console.log(sdata);
        // console.log("count=", count);
    }
    return sdata
}

async function LoadData(req,browser,page) {
    let sel = "div[class='x1i10hfl xjbqb8w x6umtig x1b1mbwd xaqea5y xav7gou x9f619 x1ypdohk xt0psk2 xe8uvvx xdj266r x11i5rnm xat24cr x1mh8g0r xexx8yu x4uap5 x18d9i69 xkhd6sd x16tdsg8 x1hl2dhg xggy1nq x1a2a7pz xt0b8zv xzsf02u x1s688f']";

    // let sel2 = "[role=article] > div > div > div > div > div > div:nth-child(2) > div > div > div:nth-child(3)";
    // let sel3 = "[role=article] span[id] span[class='x4k7w5x x1h91t0o x1h9r5lt x1jfb8zj xv2umb2 x1beo9mf xaigb6o x12ejxvf x3igimt xarpa2k xedcshv x1lytzrv x1t2pt76 x7ja8zs x1qrby5j'] a[class='x1i10hfl xjbqb8w x6umtig x1b1mbwd xaqea5y xav7gou x9f619 x1ypdohk xt0psk2 xe8uvvx xdj266r x11i5rnm xat24cr x1mh8g0r xexx8yu x4uap5 x18d9i69 xkhd6sd x16tdsg8 x1hl2dhg xggy1nq x1a2a7pz x1heor9g xt0b8zv xo1l8bm']";
    let count = 0;
    let sdata = [];
    page.on('console', async (msg) => {
        const msgArgs = msg.args();
        for (let i = 0; i < msgArgs.length; ++i) {
            console.log(await msgArgs[i].jsonValue());
        }
    });

    function sleep(ms) {
        console.log('sleeping')
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    let searchLimit = req.body.searchLimit;
    const startedAt = Date.now();
    while (count < searchLimit) {
        console.log(' while looping');
        if (Date.now() - startedAt > 1000 * 20) {
            console.log('timeout');
            break;
        }
        let x = await page.evaluate(async () => {
            window.scrollBy(0, 100);
            if (document.querySelector("[role=feed] [class='x1n2onr6 x1ja2u2z x9f619 x78zum5 xdt5ytf x2lah0s x193iq5w xz9dl7a']")) {
                return 0;
            }
            if (document.querySelector("img[src='https://static.xx.fbcdn.net/rsrc.php/y_/r/Krj1JsX3uTI.svg?_nc_eui2=AeFlHmecVHfBL0GiEFOTScAfejgaiNC5All6OBqI0LkCWcWpgMfkyH4KFDta9BQPSGrdYyY4wuznAO3z7VlIjomN']")) {
                return 0;
            }

            return 1;
        });

        if (x == 0) {
            break;
        }

        await sleep(200);

        await page.evaluate(async (sel) => {
            let elements = Array.from(document.querySelectorAll(sel));
            elements.forEach((item) => {
                item.click();
            });
        }, sel);

        ({count, sdata} = await page.evaluate(() => {
            let all_data = [];
            all_data = Array.from(document.querySelectorAll("[role=article]")).map((item) => {
                let data = {};
                let first_part = item.querySelector(":scope > div > div > div > div > div > div:nth-child(2) > div > div > div:nth-child(2)");
                let second_part = item.querySelector(":scope > div > div > div > div > div > div:nth-child(2) > div > div > div:nth-child(3)");
                data.text = "";
                if (second_part == null) {
                    return null;
                }
                if (first_part == null) {
                    return null;
                }
                let title = first_part.querySelector("h3[id] a");
                let subtitles = Array.from(first_part.querySelectorAll("span[id] span:not([class]) a"));
                if (subtitles.length > 1) {
                    data.profile_link = subtitles[0].href;
                    data.post_link = subtitles[1].href;
                } else if (subtitles.length == 1) {
                    data.post_link = subtitles[0].href;
                    data.profile_link = title.href;
                } else {
                    data = null;
                    return data;
                }
                if (second_part.children.length > 0 && !second_part.children[0].hasAttribute('id')) { // is not image
                    if (second_part.children[0].getAttribute('class') != "") { // sub-post
                        if (second_part.children[0].children[0].querySelector(":scope > div[dir=auto]") == null) {
                            return null;
                            // throw new Error('dd1');
                        }
                        data.text += second_part.children[0].children[0].querySelector(":scope > div[dir=auto]").textContent + "\n";
                    } else {
                        if (second_part.children[0] == null) {
                            return null;
                            // throw new Error('dd2');
                        }
                        data.text += second_part.children[0].textContent;
                    }
                }
                if (second_part.children.length > 1 && !second_part.children[1].hasAttribute('id')) { // is not image
                    if (second_part.children[1].getAttribute('class') != "") {
                        if (second_part.children[1].children[0].querySelector(":scope > div[dir=auto]") == null) {
                            return null;
                            // throw new Error('dd3' +  first_part.post_link);
                        }
                        data.text += second_part.children[1].children[0].querySelector(":scope > div[dir=auto]").textContent;
                    } else {
                        if (second_part.children[1] == null) {
                            return null;
                            throw new Error('dd4');
                        }
                        data.text += second_part.children[1].textContent;
                    }
                }

                return data;
            })
            all_data = all_data.filter(item => {
                return item != null;
            });

            let count = all_data.length;
            return {count, sdata: all_data};

        }));
    }
    console.log('page closed');
    await page.close();
    let xdata = [];
    for (let item of sdata) {
        console.log('item=', item);
        page = await browser.newPage();
        await page.setDefaultNavigationTimeout(0);
        await page.goto(item.profile_link, {waitUntil: "networkidle2", timeout: 0});

        let city = await page.evaluate(() => {
            let e = document.querySelector("div[class='x9f619 x1n2onr6 x1ja2u2z x78zum5 x2lah0s x1qughib x6s0dn4 xozqiw3 x1q0g3np'] > div[class='x9f619 x1n2onr6 x1ja2u2z x78zum5 xdt5ytf x2lah0s x193iq5w xeuugli']");
            if (e) {
                e.click();
            }
            let x = document.querySelector("div[class='x78zum5 xdt5ytf x1iyjqo2 x1n2onr6'] > div[class='x4k7w5x x1h91t0o x1beo9mf xaigb6o x12ejxvf x3igimt xarpa2k xedcshv x1lytzrv x1t2pt76 x7ja8zs x1n2onr6 x1qrby5j x1jfb8zj'] a");
            if (x) {
                x.click();
            }
            let y = document.querySelector("img[src='https://static.xx.fbcdn.net/rsrc.php/v3/y5/r/VMZOiSIJIwn.png?_nc_eui2=AeF83LbZBKKn3uYtYjn3OcyGysO07LK9kRPKw7Tssr2RE77QWzkWrBNk-fIyTJluVdn2ltA6S71UD8ATh6jI2L4u']");
            if (y) {
                if (y.parentElement.parentElement.querySelector(":scope > div[class='x9f619 x1n2onr6 x1ja2u2z x78zum5 xdt5ytf x193iq5w xeuugli x1r8uery x1iyjqo2 xs83m0k xamitd3 xsyo7zv x16hj40l x10b6aqq x1yrsyyn']") == null) {
                    throw new Error('dd5');
                }
                return y.parentElement.parentElement.querySelector(":scope > div[class='x9f619 x1n2onr6 x1ja2u2z x78zum5 xdt5ytf x193iq5w xeuugli x1r8uery x1iyjqo2 xs83m0k xamitd3 xsyo7zv x16hj40l x10b6aqq x1yrsyyn']").textContent;
            }
            let z = document.querySelector("img[src='https://static.xx.fbcdn.net/rsrc.php/v3/yW/r/8k_Y-oVxbuU.png?_nc_eui2=AeFxXyXIUKzKuNmXH1nUn0XWAMO_1wPOzQ4Aw7_XA87NDhIikw3fYmgZgfgWF15jqCf6YZw9AnJZxaSSvinlQqO9']");
            if (z) {
                if (z.parentElement.parentElement.querySelector(":scope > div[class='x9f619 x1n2onr6 x1ja2u2z x78zum5 xdt5ytf x193iq5w xeuugli x1r8uery x1iyjqo2 xs83m0k xamitd3 xsyo7zv x16hj40l x10b6aqq x1yrsyyn']") == null) {
                    throw new Error('dd6');
                }
                return z.parentElement.parentElement.querySelector(":scope > div[class='x9f619 x1n2onr6 x1ja2u2z x78zum5 xdt5ytf x193iq5w xeuugli x1r8uery x1iyjqo2 xs83m0k xamitd3 xsyo7zv x16hj40l x10b6aqq x1yrsyyn']").textContent;
            }
            return "";
        });
        await page.close();
        xdata.push({
            ...item,
            city
        });
    }
    // await browser.close();
    return xdata;
}

async function get_posts(req, browser, page) {

    await page.goto("https://www.facebook.com/search/posts?q=" + req.body.keyword, {
        waitUntil: "networkidle2",
        timeout: 0
    });
    return await LoadData(req, browser, page);
}

async function get_latest_posts(req, browser, page) {

    await page.goto("https://www.facebook.com/search/posts?q=" + req.body.keyword + "&filters=eyJyZWNlbnRfcG9zdHM6MCI6IntcIm5hbWVcIjpcInJlY2VudF9wb3N0c1wiLFwiYXJnc1wiOlwiXCJ9In0%3D", {
        waitUntil: "networkidle2",
        timeout: 0
    });

    return await LoadData(req, browser, page);
}

module.exports = router;
