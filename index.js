const express = require('express');
const bodyParser = require('body-parser');
const methodOverride = require('method-override')
const expressCors = require('express-cors');
const app = express()
const port = process.env.PORT || 3000
const timeout = process.env.TIMEOUT || 100;
const {logErrors, clientErrorHandler, errorHandler} =require('./error/handler');

app.use(expressCors());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
const router = require('./routers/facebookRouter');
app.use(bodyParser.json())
app.use(express.json())
app.use(methodOverride())
app.use(logErrors)
app.use(clientErrorHandler)
app.use(errorHandler)

app.use('/facebook', router);

const server = app.listen(port, () => {
    console.log('server is running on host http://localhost:' + port)
})

server.headersTimeout = timeout * 1000;
server.keepAliveTimeout = timeout * 1000;
server.timeout = timeout * 1000;

