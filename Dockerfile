#  install nodejs and chromium
FROM shivjm/node-chromium:node19-chromium115-alpine
#  create app directory

WORKDIR /usr/src/app

#  install app dependencies

COPY package*.json ./

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=1

RUN npm install --silent

# make Puppeteer use correct binary:

ENV PUPPETEER_EXECUTABLE_PATH=/usr/bin/chromium-browser

#  bundle app source
COPY . .
# expose port and start application
EXPOSE 3000
# set environment to production
ENV NODE_ENV=production
# run the application
CMD ["npm", "dev","--quiet"]